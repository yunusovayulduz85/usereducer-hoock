import React, { useEffect, useState } from 'react'
import EditTask from './EditTask'
import {useDrag} from "react-dnd"
function ToDo({ task ,index,taskList,setTaskList}) {
    const [time,setTime]=useState(0);
    const [running,setRunning]=useState(false);
    const [{isDragging},drag]=useDrag(()=>({
        type:"todo" ,
        collect:(monitor)=>({
            isDragging: !!monitor.isDragging
        })
    }))
    const deleteTask=()=>{
        let deltedTaskIndex=taskList.indexOf(task);
        taskList.splice(deltedTaskIndex,1);
        setTaskList([...taskList])
    }
    useEffect(()=> {
        let interval;
        if(running){
           interval= setInterval(()=>{
            setTime(prev=>prev+=10)
           },10)
        }
        else if(!running){
            clearInterval(interval)
        }
        return ()=>clearInterval(interval)
    },[running])
    return (
        <   >
            <div className='mx-3 p-3 w-50 bg-white mb-4' ref={drag}>
                <div className='d-flex items-center justify-content-between'>
                    <p className='font-semibold text-xl fw-bold fw-medium'>{task.projectName}</p>
                    <EditTask task={task} index={index} taskList={taskList} setTaskList={setTaskList}/>
                </div>

                <p className='text-lg py-2'>{task.taskDescription}</p>
                <div className='d-flex items-center'>
                    <div>
                        <span>
                            {("0"+Math.floor((time/3600000)%24)).slice(-2)}:
                        </span>
                        <span>
                            {("0"+Math.floor((time/60000)%60)).slice(-2)}:
                        </span>
                        <span>
                            {("0"+Math.floor((time/1000)%60)).slice(-2)}:
                        </span>
                        <span className='text-sm'>
                            {("0"+Math.floor((time/10)%100)).slice(-2)}
                        </span>
                    </div>
                    <div className='mx-4'>
                        {
                            running ? (<button onClick={()=>setRunning(false)} className=' mx-2'>Stop</button>):(<button onClick={()=>setRunning(true)} className=''>Start</button>)
                        }
                        <button onClick={()=>setTime(0)}>Reset</button>
                    </div>
                </div>
                <div className='w-100 d-flex justify-content-center'>
                    <button className='btn btn-danger' onClick={deleteTask}>Delete</button>
                </div>
            </div>
        </>

    )
}

export default ToDo