import React from 'react'
import { useState } from 'react'

function AddTask({taskList,setTaskList}) {
    const [addModal, setAddModal] = useState(false);
    const [projectName,setProjectName]=useState("");
    const [taskDescription,setTaskDescription]=useState("");
    const [errorMassage,setErrorMassage]=useState("");
   
    const handleInput=e=>{
        const {name,value}=e.target;
        if(name==="projectName") {
            setProjectName(value)
            setErrorMassage("");
        };
        // if(name==="projectName" && value==""){
        //     setErrorMassage("Enter project name to continue")
        // }
        if (name ==="taskDescription") setTaskDescription(value);
    }
    const handleAdd = (e) => {
        e.preventDefault();
        if(!projectName){
            setErrorMassage("Enter project name to continue");
        }else{
            // let timeStamp=new Date();
            // let tempList=taskList;
            // tempList.push({
            //     projectName,
            //     taskDescription,
            //     timeStamp,
            //     duration:0
            // })
            // localStorage.setItem("taskList",JSON.stringify(tempList));
            // window.location.reload();
            setTaskList(
            [...taskList, { projectName, taskDescription }]
        )
        setAddModal(false);
        setProjectName("");
        setTaskDescription("")
        }
        
    }
    return (
        <div>
            <button className='btn btn-info' type='button' onClick={() => setAddModal(true)}>+ New</button>
            {
                addModal ? (
                    <>
                    
                        <div className='position-absolute border rounded rounded-3 z-100 content'>
                            <div className='bg-white'>
                                <div className='d-flex justify-between py-3 px-4 items-center'>
                                    <h3 className='mx-5'>Add New Task</h3>
                                    <button className='btn btn-danger mx-5' onClick={() => setAddModal(false)}>x</button>
                                </div>
                                <form className='p-6'>
                                    <div className='px-4'>
                                        <label className=' mb-2' htmlFor='project-name font'>Project Name</label>
                                        <br />
                                        <input id="project-name"
                                        value={projectName}
                                        onChange={handleInput}
                                        name='projectName'
                                         type='text'
                                          placeholder='Project name' className='w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-4 leading-tight focus:outline-none focus:bg-white' required />
                                        <p className='text-danger '>{errorMassage}</p>
                                    </div>

                                    <div className='px-4'>
                                        <label className='track-wide uppercase text-gray-700 text-xs font-semibold mb-2' htmlFor='project-name'>
                                            Task Description
                                        </label>
                                        <br/>
                                        <textarea id="task-description"
                                         cols="30"
                                         rows="3"
                                         name="taskDescription"
                                        value={taskDescription}
                                        onChange={handleInput}
                                         placeholder='Task description'
                                            className='w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-5 leading-tight focus:outline-none focus:bg-white'
                                        />
                                    </div>
                                </form>
                                <div className='d-flex justify-content-end p-4'>
                                    <button className='btn btn-success ' onClick={handleAdd}>Add Task</button>
                                </div>
                            </div>


                        </div>
                    </>
                ) : (null)
            }
        </div>
    )
}

export default AddTask