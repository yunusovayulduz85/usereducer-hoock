import React, { useReducer } from 'react'
// import { useState } from 'react'
const logInReducer = (state, action) => {
  switch (action.type) {
    case "field": {
      return {
        ...state,
        [action.fieldName]: action.payload
      }
    }
    case "logIn": {
      return {
        ...state,
        error: "",
      }
    }
    case "success": {
      return {
        ...state,
        check: true,
        password: "",
      }
    }
    case "error": {
      return {
        ...state,
        error: "Incorrect Username or Password",
        check: false,
        userName: "",
        password: ""
      }
    }
    case "logOut": {
      return {
        ...state,
        check: false,
      }
    }
    default: return state;
  }
}
function App() {
  // const [userName, setUserName] = useState("");
  // const [password, setPassword] = useState("");
  // const [check,setCheck]=useState(false);
  // const [error,setError]=useState("");
  const [state, dispatch] = useReducer(logInReducer, {
    userName: "",
    password: "",
    setCheck: false,
    error: "",
  })
  const handleSubmit = e => {
    e.preventDefault();
    // setError("")
    dispatch({ type: "logIn" })
    try {
      if (state.userName == "Yulduz" && state.password == "2005") {
        dispatch({ type: "success" })
        // setCheck(true)
      } else {
        throw  Error;

      }
      // setPassword("");
    } catch (error) {
      dispatch({ type: "error" })
      // setError("Incorrect Username or Password");
      // setUserName("");
      // setPassword("");
    }
  }
  return (
    <div>
      <h1 className='text-center'>10-useReducer</h1>
      <div>
        {
          state.check ? (
            <>
              <h1 className='text-center'>Welcome {state.userName}</h1>
              <div className='d-flex align-items-center justify-content-center'>
                {/* <button className='btn btn-info' onClick={()=>setCheck(false)}>Back</button> */}
                <button className='btn btn-info' onClick={() => dispatch({ type: "logOut" })}>Back</button>
              </div>
            </>
          ) : (
            <>
                <form
                  className=
                  'd-flex flex-column align-items-center justify-content-center'
                  onSubmit={handleSubmit}
                >
                  <input
                    type='text'
                    autoComplete='username'
                    placeholder='Username..'
                    value={state.userName}
                    // onChange={(e) => setUserName(e.target.value)}
                    onChange={(e) => dispatch({
                      type: "field",
                      fieldName: "userName",
                      payload: e.target.value
                    })}
                    className='mt-3 border border-2 rounded p-1'
                  />
                  <input
                    type='password'
                    autoComplete='current-password'
                    placeholder='Password..'
                    value={state.password}
                    // onChange={(e) => setPassword(e.target.value)}
                    onChange={(e) => dispatch({
                      type: "field",
                      fieldName: "password",
                      payload: e.target.value
                    })}
                    className='mt-3  border border-2 rounded p-1'
                  />
                  <button
                    className='btn btn-success mt-3'
                    type='submit'
                  >
                    Submit
                  </button>
                  <p className='text-danger mt-2'>{state.error}</p>
                </form>
             
            </>
          )
        }

      </div>
    </div>
  )
}

export default App

//** useReducer ichiga ikkita qiymat qabul qiladi birinchisi funksiya(param1,action (action bilan dispatch dan keladigan type yoki age qiymatlarni olishimiz mumkin )) 2-si o'sha funksiyaning 1-parametri;